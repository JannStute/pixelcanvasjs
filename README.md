# Features
- automatic fingerprint extraction
- automatic wait time acquisition on run
- optional beep on place
- queued execution
- drawing lines
- filling areas
- randomizing placement order
- built-in help text

# Changelog
## v1.1
- custom sorting functionality

# How To Use
1. drag / copy the contents of the .js file into the console of your browser dev tools and press enter
2. create a new pixelcanvasjs object
3. queue any placements you want to make
4. execute the `run()` method of the object

(you can add more to the queue while it is working on the existing stuff, if you add to the queue after it is done just execute run() again)

# Queueing Placements
## Lines
Using the `horizontalLine(x1,x2,y,color)` or the `verticalLine(y1,y2,x,color)` methods to draw an orange line:
```js
let obj = new pixelcanvasjs();
obj.horizontalLine(0,2,1,6); // includes the following points: (0,1), (1,1), (2,1)
obj.verticalLine(3,5,2,6); // includes the following points: (2,3), (2,4), (2,5)
```

## Filling an Area
Using the `fill(x1,y1,x2,y2,color)` method to fill an area:
```js
let obj = new pixelcanvasjs();
obj.fill(1,2,3,4,5) // includes the following points: (1,3) (2,3) (1,4) (2,4)
```

## Single Pixel
Queueing 2 single pixel placements by assigning to `obj.queue`:
```js
let obj = new pixelcanvasjs();
obj.queue = {x: 69, y: 69, color: 7} // obj._queue: [{x:69, y:69, color:7}]
obj.queue = {x: 420, y: 420, color: 5} // obj._queue: [{x:69, y:69, color:7}, {x:420, y:420, color:5}]
```

# Full Example
This fills the area from (69,420) to (420,69) with cyan in a random order and then places a red pixel at (200, 200) (inside the filled area). After every placed pixel it will beep once at half volume. The fingerprint will be extracted automatically.
```js
let obj = new pixelcanvasjs(true);
obj.volume = 0.5;
obj.fill(69,420, 420,69, 12);
obj.randomizeQueue();
obj.queue = {x: 200, y: 200, color: 5};
obj.run();
```

## custom fingerprint, no beep
```js
let obj = new pixelcanvasjs(false, "this is the 32 chars fingerprint");
obj.volume = 0.5;
obj.fill(69,420, 420,69, 12);
obj.randomizeQueue();
obj.queue = {x: 200, y: 200, color: 5};
obj.run();
```

# Color Codes
Color Code | Color | Color Code | Color 
-----------|-------|------------|-------
0          | White |  8         | Yellow
1      | Light Gray|  9         | Light Green
2          | Gray  | 10         | Green
3          | Black | 11         | Light Blue
4          | Rose  | 12         | Cyan
5          | Red   | 13         | Blue
6          | Orange| 14         | Lavender
7          | Brown | 15         | Purple

# Internal Workings of Pixelcanvas.io
Here I am just going to list a few things I found out while reverse engineering the webpage.

## Fingerprints
The fingerprint is generated using the FingerprintJS Library, with a custom FingerprintJS-API endpoint at "https://pixelcanvas.io/api/me".

## Placing Pixels
The following code is everything needed to send a correct request to place a pixel (when executed on pixelcanvas.io).
```js
const body = {
    x: x,
    y: y,
    color: color,
    fingerprint: fingerprint,
    token: null,
    wasabi: x + y + 2342
};
const { success, waitSeconds } = await client.mutation('pixel', body);
```
